标题
Magica Voxel 引导 | 软件下载、降噪插件、汉化等

官网：  
https://ephtracy.github.io/index.html?page=mv_main

软件下载：
https://ghproxy.com/  

https://github.com/ephtracy/ephtracy.github.io/releases/download/0.99.7/MagicaVoxel-0.99.7.0-win64.zip

https://ghproxy.com/https://github.com/ephtracy/ephtracy.github.io/releases/download/0.99.7/MagicaVoxel-0.99.7.0-win64.zip

翻译：
https://github.com/ephtracy/voxel-model/tree/master/language/chinese  

https://pan.fastmirror.net/s/qG0Ua?path=%2F

插件：
https://github.com/ephtracy/ephtracy.github.io/releases/download/0.01/plugin-intel-denoiser-win64-1.2.0.zip

需要使用第三方代理下载仓库中特定文件夹的方法

---










> Magica Voxel 在国内外的教程资源都十分丰富，但是由于这款软件的发布强依赖 Github 相关服务（官网、下载、汉化等等），前些年先，Github 在国内还能较为正常的使用，如今已难以直接使用。本文章主要解决一些网络相关问题以及一些其他教程中尚未提到的方面

该篇是一篇推广性无商教程，如若您有一定的计算机使用基础，可以跳过大多文字，只看大多步骤中的引用框中的文字即可

---
## 下载链接

软件官网：  
https://ephtracy.github.io/index.html?page=mv_main  
> 官网使用的是 Github Pages 服务，在国内大多地区无法直接访问，或者访问较慢，不过官网访问不了影响不大，没太多东西

软件官方下载链接：  
Windows 0.99.7：  https://github.com/ephtracy/ephtracy.github.io/releases/download/0.99.7/MagicaVoxel-0.99.7.0-win64.zip  
MacOS 10.15 0.99.6.2：  https://github.com/ephtracy/ephtracy.github.io/releases/download/0.99.6/MagicaVoxel-0.99.6.2-macos-10.15.zip  
MacOS 10.7 0.99.6.2：  https://github.com/ephtracy/ephtracy.github.io/releases/download/0.99.6/MagicaVoxel-0.99.6.2-macos-10.7.zip  
**软件第三方加速下载链接**：  
Windows 0.99.7: https://ghproxy.com/https://github.com/ephtracy/ephtracy.github.io/releases/download/0.99.7/MagicaVoxel-0.99.7.0-win64.zip  
MacOS 10.15 0.99.6.2：  https://ghproxy.com/https://github.com/ephtracy/ephtracy.github.io/releases/download/0.99.6/MagicaVoxel-0.99.6.2-macos-10.15.zip    
MacOS 10.7 0.99.6.2：  https://ghproxy.com/https://github.com/ephtracy/ephtracy.github.io/releases/download/0.99.6/MagicaVoxel-0.99.6.2-macos-10.7.zip  
> 官方软件的下载使用的是 Github release 服务，在国内大多地区无法下载，或者下载非常慢  
> 使用第三方加速服务便可正常下载，这里使用ghproxy.com服务  

汉化下载链接：  
https://github.com/ephtracy/voxel-model/tree/master/language/chinese  
**汉化第三方镜像**：  
https://pan.fastmirror.net/s/qG0Ua?path=%2F  
> 官方汉化是直接放在 Github 仓库中的，且汉化文件有多个，如若直接下载非常麻烦，目前还没有找到 使用第三方代理下载 仓库中特定文件夹的方法，因此暂时使用国内网盘重新上传了一个镜像，其实这是违反官方协议的;;

降噪插件下载链接：  
Windows：  
https://github.com/ephtracy/ephtracy.github.io/releases/download/0.01/plugin-intel-denoiser-win64-1.2.0.zip  
MacOS：  
https://github.com/ephtracy/ephtracy.github.io/releases/download/0.01/plugin-intel-denoiser-macos-1.2.0.zip  
**降噪插件第三方加速下载链接**：  
Windows：  
https://ghproxy.com/https://github.com/ephtracy/ephtracy.github.io/releases/download/0.01/plugin-intel-denoiser-win64-1.2.0.zip  
MacOS：  
https://ghproxy.com/https://github.com/ephtracy/ephtracy.github.io/releases/download/0.01/plugin-intel-denoiser-macos-1.2.0.zip  
> 降噪插件官方使用的也是 Github release 服务，同样使用第三方加速即可

---

## 软件下载安装
> Magica Voxel 是一款便携版/绿色软件，不需要安装，直接下载压缩包并解压到一个目录下就能使用了。这里以 Windows 版作演示

1、从上面的 **下载链接** 处下载好软件的压缩包。对于版本选择，Windows 用户建议选择最新版本（0.99.7），MacOs 用户选择适合自己系统的最新版本即可（最高0.99.6.2，其中 MacOS 的版本有区分 10.7 和 10.15 ）  
2、将下载好的压缩包复制到你计划放 MagicaVoxel 软件的位置，建议单独给他分配一个文件夹喔～  
![](/imgs/others/mvdownload/copy.png)  
3、右键刚刚复制的压缩包，选择 全部解压缩，在弹出来的弹窗中，将路径后面的 \MagicaVoxel-0.99.7.0-win64 删除掉（例如 H:\MagicaVoxel\MagicaVoxel-0.99.7.0-win64 改成 H:\MagicaVoxel），然后直接点击 提取 即可  
![](/imgs/others/mvdownload/unzip.png)  
![](/imgs/others/mvdownload/confirm.png)  
![](/imgs/others/mvdownload/dirdelmv.png)  

至此，软件已经下载好了，直接点击刚刚打开的目录下的 MagicaVoxel.exe 即可启动

## 汉化下载安装
> 汉化就是直接替换词典文件和字体文件，因此所有系统的汉化文件都是一样的。该目录位于根目录下的 /config 文件夹中，汉化压缩包直接解压在该目录即可  

1、从上面的 **下载链接** 处下载好汉化压缩包。各个操作系统的汉化文件都是同一个  
2、在 MagicaVoxel 的目录下找到 /config 目录，将下载好的汉化压缩包复制过去  
3、右键刚刚复制的压缩包，选择 全部解压缩，在弹出来的弹窗中，将路径后面的 \官方汉化包 删除掉（例如  H:\MagicaVoxel\MagicaVoxel-0.99.7.0-win64\config\官方汉化包 改成  H:\MagicaVoxel\MagicaVoxel-0.99.7.0-win64\config），然后直接点击 提取 即可，这时候弹出来窗口直接选择 替换目标中的文件 即可  
![](/imgs/others/mvdownload/dirdeli18n.png)  
![](/imgs/others/mvdownload/overwrite.png)  

## 降噪插件下载安装
> 降噪插件的安装也比较简单，直接将压缩包解压到 MagicaVoxel 的根目录下即可

1、从上面的 **下载链接** 处下载好降噪插件压缩包。各个操作系统的降噪插件不同，但不同于软件本体，仅区分系统类型，MacOS 内没有区分  
2、直接打开 MagicaVoxel 的目录，将下载好的降噪插件压缩包直接复制过去  
3、右键刚刚复制的压缩包，选择 全部解压缩，在弹出来的弹窗中，将路径后面的 \plugin-intel-denoiser-win64-1.2.0 删除掉（例如  H:\MagicaVoxel\MagicaVoxel-0.99.7.0-win64\plugin-intel-denoiser-win64-1.2.0 改成  H:\MagicaVoxel\MagicaVoxel-0.99.7.0-win64），然后直接点击 提取 即可，这时降噪便已安装好了

# 界面缩放调整
> MagicaVoxel 没有图形化的配置界面，但是可以通过直接调整 /config 目录下的配置文件进行配置。MagicaVoxel 在高分辨率显示器上的界面缩放比例会比较小，通过配置 config.txt 中的 ui_scale 即可调大调小

1、打开 MagicaVoxel 文件夹中的 /config 文件夹  
2、找到 config.txt 文件并打开  
3、找到 ui_scale 的选项，修改后面的数字，数字越大，界面越大，反之亦然。**注意，修改范围为[0.5, 3.0]，且不能修改除了该数字意外的其他任何一个字符，否则可能导致出错**  
4、关闭保存 config.txt，重新打开 MagicaVoxel 软件即生效  

![](/imgs/others/mvdownload/config.png)  

修改之前（2.5K显示器）：  
![](/imgs/others/mvdownload/default.png)  
修改之后（ui_scale 改为2.0）：  
![](/imgs/others/mvdownload/modified.png)  

---
本文贡献者：[壹粥](https://space.bilibili.com/474001515)（文章编写 审查）
> 本文地址：https://gitee.com/Lianmoe/minecraft-community-tutorial/blob/master/%E5%85%B6%E4%BB%96%E6%95%99%E7%A8%8B/MagicaVoxel%E5%BC%95%E5%AF%BC.md

![](/imgs/set/open_community.png)

这是一篇使用 CC BY-NC-SA 4.0 协议开源、开放贡献的文章，如果对您有帮助，请帮忙转发、点赞喔，以帮助到更多的人～
