目前最快捷的安装 FLASH 的方案

> 太长不看：简单来说，只需要从 github 上下载别人制作的去除了广告的中国版 flash，并使用支持 flash 的浏览器（IE浏览器、低版本的chrome或火狐、EDGE的IE模式、水狐这类属于低版本火狐内核的第三方浏览器），而EDGE的IE模式对于大多人的电脑来说是最合适的

---

## 一、安装 Clean Flash 
> FLASH 安装中会出什么问题？FLASH 在国内是由重橙网络合作代理，这家公司在国内版 flash 中插入了大量的弹窗广告，影响电脑使用体验。而如若你想安装国际版 flash，他们的自动检测地区功能又会阻止你使用国际版。因此，有人修改了中国版 flash，去除了广告，并发布了在 github 上，叫做 CleanFlashInstaller 只需要安装这个版本便可以解决上面的问题

#### 1、下载 Clean Flash 安装包  
可以从官方 github 仓库下载，但是国内直接访问会有问题，所以我重新用国内网盘传了一下，可以自行选择使用哪一个下载  
[官方 Github 下载直链](https://github.com/Aira-Sakuranomiya/CleanFlashInstaller/releases/download/34.0.0.277/CleanFlash_34.0.0.277_Installer.exe)  
[国内网盘下载直链](https://pan.fastmirror.net/api/v3/file/source/3099/CleanFlash_34.0.0.277_Installer.exe?sign=MwOQ1E59UXmFLQb2DczfuRtfUz_EPZ7nGv0NMFrAz_o%3D%3A0)  

#### 2、安装 Clean Flash
双击安装包进行安装，所有有框框可以选中的都选中，每一步骤都按右下角按钮即可


## 二、准备支持 flash 的浏览器
> flash 本身已经不受支持了，即使安装好了，主流新浏览器（EDGE、chrome、火狐等）都早已不能使用 flash，有很多解决方案，最方便的方案还是使用EDGE的IE模式功能
> “Chrome 浏览器版本需要低于 87，火狐浏览器版本需要低于 84”
> ![1](/imgs/others/flash/version.jpg)

这需要分很多种情况：  
- 如果电脑上有 EDGE 浏览器，那么直接使用 EDGE 的 IE 模式即可；
- 如果电脑上没有 EDGE 浏览器，平时使用的是 360、QQ 这些国内浏览器，那么可以直接使用；¹
- 如果电脑上没有 EDGE 浏览器，也没有其他国产浏览器，那么只能安装使用旧版火狐或chromium内核的浏览器，推荐安装[水狐](https://www.waterfox.net/download/)²

第二第三种办法都比较简单，本教程着重讲解第一种办法：
#### 1、为 EDGE 浏览器开启 IE 模式功能
如图，在右上角三个点的位置中找到设置，并 设置 - 默认浏览器 中将 IE 模式项改为允许
![1](/imgs/others/flash/iemodeon1.png)  
![1](/imgs/others/flash/iemodeon2.png)  
![1](/imgs/others/flash/iemodeon3.png)

## 三、完成
如图，当要访问的网站需要使用 flash 时，在右上角三个点的位置找到 IE 模式，开启后，同时在地址栏旁的 IE 模式设置中打开 下次以IE模式 启动，这样这个网站以后就会自动启用 ie 模式，也就能使用 Flash 了  
![1](/imgs/others/flash/ie.png)  
![](/imgs/others/flash/iealways.png)



>注释：
>1. 这个不确定
>2. 实际上这个也不确定，这是 CleanFlashIntaller 中提示的，可能后来水狐也更新内核了

---
本文贡献者：[壹粥](https://space.bilibili.com/474001515)（文章编写 审查）、[Zachery_Liu](https://www.wkn.icu/)（方案讨论）  
> 本文地址：https://gitee.com/Lianmoe/minecraft-community-tutorial/blob/master/%E5%85%B6%E4%BB%96%E6%95%99%E7%A8%8B/%E7%9B%AE%E5%89%8D%E6%9C%80%E5%BF%AB%E6%8D%B7%E7%9A%84%E5%AE%89%E8%A3%85%20FLASH%20%E7%9A%84%E6%96%B9%E6%B3%95.md









