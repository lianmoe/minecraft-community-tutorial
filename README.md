### Minecraft 教程  
由社区维护的 Minecraft 各个方面的教程！

目前正在逐步完善中...

## 如何参与编写 
文章使用 Markdown 语言编写，这是一种标记语言，简单且实用，之所以使用 Markdown，是因为这样的 md 文件更适合协作，更适合移植，不过不会这种语法的写者也不必担心，您可以使用[Stackedit 在线编辑器](https://stackedit.cn/) ，像编写 Word 文档一样按按钮编写 Markdown 即可  

具体的教程编写引导、编写要求等请参考 [内容编写指南](https://gitee.com/Lianmoe/minecraft-community-tutorial/blob/master/%E5%85%B6%E4%BB%96%E6%95%99%E7%A8%8B/%E5%86%85%E5%AE%B9%E7%BC%96%E5%86%99%E6%8C%87%E5%8D%97.md) 。


## 贡献者名单
<!-- readme: contributors -start -->
<table>
<tr>
    <td align="center">
        <a href="https://github.com/Zachery-Liu">
            <img src="https://avatars.githubusercontent.com/u/61861732?v=4" width="100;" alt="Zachery-Liu"/>
            <br />
            <sub><b>Zachery Liu</b></sub>
        </a>
    </td>
    <td align="center">
        <a href="https://github.com/Yizhouuu">
            <img src="https://avatars.githubusercontent.com/u/107225762?v=4" width="100;" alt="Yizhouuu"/>
            <br />
            <sub><b>壹粥</b></sub>
        </a>
    </td></tr>
</table>
<!-- readme: contributors -end -->

## 声明
开源：Minecraft 社区教程项目使用 [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.zh) 开源  
开放贡献：Minecraft 社区教程项目以 Gitee 平台为核心进行开放贡献，更多相关内容详见开放贡献引导

## 开放贡献
欢迎您参与本项目的开放贡献！

为了保证项目质量和合法性，请您遵守以下注意事项：

贡献内容必须符合 CC BY-NC-SA 4.0 协议的要求，即必须署名、非商业使用、相同方式共享。如果您对协议的理解有疑问，请参考协议全文或咨询相关法律专业人士。

请不要提交与项目主题无关的内容，包括但不限于垃圾邮件、恶意代码等。

您的贡献可能需要经过审核才能合并到项目中，我们会尽快审核并给出反馈，请耐心等待。

如果您的贡献被采纳并合并到项目中，您的贡献将被视为公共领域的作品。请确保您有权利对贡献内容进行开放贡献，或者已经获得相关权利。

感谢您的参与和支持，我们期待您的贡献！



[联萌的网站](lianmoe.cn/about)
先临时放在这里，毕竟目前算是联萌比较内部的项目，还没有太多社区的加入…… 
<p style="text-align: center">“Minecraft”  是 Mojang Synergies AB 的商标，本项目与 Mojang AB 以及 Microsoft Corporation 没有从属关系。</p>




<!-- links -->
[your-project-path]:Zachery-Liu/Lianmoe-Tutorial
[contributors-shield]: https://img.shields.io/github/contributors/Zachery-Liu/Lianmoe-Tutorial.svg?style=for-the-badge
[contributors-url]: https://github.com/Zachery-Liu/Lianmoe-Tutorial/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/Zachery-Liu/Lianmoe-Tutorial.svg?style=for-the-badge
[forks-url]: https://github.com/Zachery-Liu/Lianmoe-Tutorial/network/members
[stars-shield]: https://img.shields.io/github/stars/Zachery-Liu/Lianmoe-Tutorial.svg?style=for-the-badge
[stars-url]: https://github.com/Zachery-Liu/Lianmoe-Tutorial/stargazers
[issues-shield]: https://img.shields.io/github/issues/Zachery-Liu/Lianmoe-Tutorial.svg?style=for-the-badge
[issues-url]: https://img.shields.io/github/issues/Zachery-Liu/Lianmoe-Tutorial.svg
[Kook-shield]: https://img.shields.io/static/v1?label=Kook&message=加入交流频道&color=green&style=for-the-badge
[Kook-url]: https://kook.top/wtPZIy
[doc-shield]: https://readthedocs.org/projects/lianmoe-tutorial/badge/?version=latest&style=for-the-badge
[doc-url]: https://tutorial.lianmoe.cn/zh_CN/latest/?badge=latest


<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE5MDAwNDA3MjhdfQ==
-->